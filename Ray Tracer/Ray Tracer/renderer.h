#pragma once
#include <stack>
#include <vector>
#include "gl.h"

class renderer
{
private:

	struct light{
		glm::vec4 position;
		glm::vec4 intensity;
	};

	struct Object{
		Object(){ ; }
		Object(glm::vec4 pos, glm::vec3 norm, GLfloat rad, int materialID, int typeID){
			position = pos;
			normal = norm;
			radius = rad;
			matIdx = materialID;
			type = typeID;
		}
		Object(glm::vec4 pos, glm::vec3 norm, GLfloat rad, int materialID, int typeID, int bufID, int bufSize){
			position = pos;
			normal = norm;
			radius = rad;
			matIdx = materialID;
			type = typeID;
			bufferIdx = bufID;
			numFaces = bufSize;
		}
		glm::vec4 position;
		glm::vec3 normal = glm::vec3(1.0f);
		float radius = 1.0f;
		int matIdx;
		int type = 0;
		int bufferIdx;
		int numFaces;
	};

	GLuint texBuf[3], imgTex[3];
	enum uniforms {LIGHT_POSITION = 2, LIGHT_INTENSITY, ROTATION_MATRIX, NUMBER_OF_OBJECTS};

	light point;

	GLuint shader, quadProgram, size[1], mesh[1], screenWidth, screenHeight;
	GLuint skybox[5];
	Assimp::Importer importer;

	static const int numObjects = 10;
	GLuint sphereBufs[2];
	std::vector<Object> objects;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec4> vertices;
	std::vector<glm::ivec3> indices;

	void loadCubeMap(const char*fname[6], GLuint *texID);

public:
	renderer(GLuint mainShader, GLuint skyShader, GLuint width, GLuint height);
	~renderer();

	void setLightPos(glm::vec4 pos){ point.position += pos; };
	void sceneDraw(glm::mat4 cam, glm::vec3 eye);
	void populateBuffer(const aiScene* scene);
};

