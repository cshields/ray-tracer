	#version 440

	layout (location = 0) in vec4 position;
	layout (location = 2) in vec2 inTexCoords;

	out vec2 outTexCoords;

	void main(void)
	{
		vec4 vertexPosition = position;
		outTexCoords = inTexCoords;
		gl_Position = vertexPosition;
	}